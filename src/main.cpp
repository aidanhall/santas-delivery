#include <iostream>
#include "declarations.h"


struct node
{
    char id;
    bool visited = false;
    bool started_from = false;
};

struct connection
{
    node *nodes[2];
    int size;
};

struct route
{
    int total_distance;
    char order[n_nodes];
};

// Takes a start node, a list of nodes and the connections between them.
// For each of the adjacent, unvisited nodes,
// add the distance to the node to the total length.
// add it to the current route.
// start routefinding again from it.
// When a node with no adjacent, unvisited nodes is reached,
// if there are unvisited nodes, return the previous shortest route as the route is incomplete.
// otherwise, return the new route if its total length is less than that of the shortest route.
route try_routes(struct node *start_node, struct connection paths[n_connections], struct route current_route, struct route shortest_route, int stage)
{
    bool unvisited_adjacent;
    bool any_unvisited_adjacent = false;
    int pick_node;

    current_route.order[stage] = start_node->id; // Adds the current node to the current route.


    // This node has been reached.
    std::cout<< "Going from " << start_node->id << " at depth: " << stage << std::endl;
    std::cout<< "So far we have gone this far: " << current_route.total_distance << std::endl;
    start_node->visited = true;

    // For each of the adjacent, unvisited nodes,
    for (int path_index = 0; path_index < n_connections; path_index++)
    {
	unvisited_adjacent = false;


	for ( int i = 0; i < 2; i++)
	{
	    // If it is a path to an adjacent node that hasn't been visited.
	    if (paths[path_index].nodes[i]->id == start_node->id  && !paths[path_index].nodes[(i+1)%2]->visited)
	    {
		// Selects the other node that the path connects to go to next.

		unvisited_adjacent = true;
		any_unvisited_adjacent = true;
		pick_node = (i+1)%2; // Gives the opposite node.
		std::cout<< "Going from: " << start_node->id << ", this connected path goes to " <<paths[path_index].nodes[pick_node]->id << std::endl;
	    }
	}

	// Start pathfinding from the adjacent node.
	if (unvisited_adjacent)
	{

	    std::cout<< "We haven't gone to: " << paths[path_index].nodes[pick_node]->id << std::endl;
	    // add the distance to the node to the total length.
	    current_route.total_distance += paths[path_index].size;

	    // Does pathfinding using the path.
	    shortest_route = try_routes(paths[path_index].nodes[pick_node], paths, current_route, shortest_route, stage+1);
	    // Reverts the length to go back up a layer.
	    current_route.total_distance -= paths[path_index].size;
	}

    }

    // Resets the visited status of the starting node, as after this point it is no longer needed.
    start_node->visited = false;

    if (!any_unvisited_adjacent) // If a dead end is reached (no unvisited adjacent nodes).
    {
	std::cout<< "We've reached a dead end.\n";
	if (stage == n_nodes - 1)
	{
	    std::cout<< "We've done all of the nodes.\n";
	    std::cout<< "Our current route has been " << current_route.total_distance << std::endl;
	    if (current_route.total_distance < shortest_route.total_distance)
		return current_route;
	    else
		return shortest_route;
	} else
	    return shortest_route;
    }



    return shortest_route;
}

int main()
{

    struct node locations[n_nodes];
    locations[0].id = 'a';
    locations[1].id = 'b';
    locations[2].id = 'c';
    locations[3].id = 'd';
    locations[4].id = 'e';
    
    struct connection paths[n_connections];
    paths[0].nodes[0] = &locations[0];
    paths[0].nodes[1] = &locations[1];
    paths[0].size = 10;
    paths[1].nodes[0] = &locations[0];
    paths[1].nodes[1] = &locations[2];
    paths[1].size = 8;
    paths[2].nodes[0] = &locations[1];
    paths[2].nodes[1] = &locations[2];
    paths[2].size = 6;
    paths[3].nodes[0] = &locations[2];
    paths[3].nodes[1] = &locations[3];
    paths[3].size = 2;
    paths[4].nodes[0] = &locations[2];
    paths[4].nodes[1] = &locations[4];
    paths[4].size = 8;
    paths[5].nodes[0] = &locations[3];
    paths[5].nodes[1] = &locations[4];
    paths[5].size = 12;
    paths[6].nodes[0] = &locations[1];
    paths[6].nodes[1] = &locations[3];
    paths[6].size = 4;

    struct route shortest_route;
    shortest_route.total_distance = 99999;
    struct route current_route;
    current_route.total_distance = 0;



    // Path-finding.

    // Start from each node.
    for (int current_start_location = 0; current_start_location < n_nodes; current_start_location++)
    {
	std::cout<< "\n\nStarting from " << locations[current_start_location].id << std::endl;
	shortest_route = try_routes(&locations[current_start_location], paths, current_route, shortest_route, 0);
    }

    std::cout<< shortest_route.order << std::endl;
    std::cout<< shortest_route.total_distance << std::endl;

}
