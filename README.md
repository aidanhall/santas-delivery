# Santa's Workshop

Father Christmas needs your help.  In order to get around the world, in the shortest time possible.
Santa needs to calculate the shortest path from place to place, visiting each place ONLY ONCE.

* This is my implementation.
* I don't expect you to know C++.

# How it works.

* Each node is a structure containing its id and whether it has been visited.
* Each path is a pair of nodes it connects and its length.

* Starting from each node, adjacent nodes are checked, one-by-one, and tried if they haven't been visited already.
* The nodes are moved through in a tree.

* When a node with no adjacent nodes that haven't been visited is reached, if all of the nodes have been visited, the route it was reached from is taken over the previous shortest route if it is shorter.
* Otherwise, to either of those conditions, the route is discarded and the routefinding moves back up one level in the tree.

# Disclaimers.

* I am bad at C++.
* This is inefficient code and brute force is bad.
* I don't care about your opinion; this was just a *'fun practice exercise'.*
