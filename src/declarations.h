#pragma once

struct node;
struct connection;
struct route;
const int n_nodes = 5;
const int n_connections = 7;

struct connection initialise_connection(int node_indexes, int size);
bool all_visited(struct node locations[n_nodes]);
bool any_not_started_from(struct node locations[n_nodes]);
struct route try_routes(struct node *start_node, struct connection paths[n_connections], struct route current_route, struct route shortest_route, int stage);
